﻿using UnityEngine;

public class PlanetView : MonoBehaviour
{
    #region data fields and properties
    public float ownRotationForce;
    public Rigidbody rigidbodyOfPlane;
    public Transform orbitateAround;
    [SerializeField]
    public PlanetModel planetModel;
    #endregion

    private void Start()
    {
    }
    
}
