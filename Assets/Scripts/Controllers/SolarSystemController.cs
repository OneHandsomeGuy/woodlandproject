﻿using System.Collections.Generic;
using UnityEngine;

public class SolarSystemController : MonoBehaviour
{
    #region data fields and properties
    [Header("Basic settings")]
    public int addPlanets = -1;
    public PlanetView sun;
    public PlanetView planetViewPrefab;

    private List<PlanetView> planets;
    private float xOffset = 2, magnitude, totalForce;
    private Vector3 forceToAdd;
    #endregion

    #region lifecycle methods
    /// <summary>
    /// On start add planets and then basic velocity
    /// </summary>
    void Start()
    {

        if (addPlanets <= 0)
        {
            addPlanets = UnityEngine.Random.Range(6, 15);
        }
        AddPlanets(addPlanets);
        InitializeCurrentPlanets();
    }

    /// <summary>
    /// Move planets, add constant force
    /// </summary>
    void FixedUpdate()
    {
        if (planets != null && sun != null)
        {
            foreach (PlanetView planetView in planets)
            {
                magnitude = Vector3.Magnitude(planetView.transform.position);
                if (magnitude != 0)
                {
                    totalForce = -(sun.rigidbodyOfPlane.mass) / (magnitude * magnitude);
                    forceToAdd = (planetView.transform.position).normalized * totalForce;
                    if (planetView.rigidbodyOfPlane != null && forceToAdd != planetView.transform.position)
                    {
                        planetView.rigidbodyOfPlane.AddForce(forceToAdd);
                    }
                }
            }
        }
    }
    #endregion

    #region data methods
    /// <summary>
    /// Adds initial velocity to every planet
    /// </summary>
    private void InitializeCurrentPlanets()
    {
        if (planets != null && sun != null)
        {
            foreach (PlanetView planetView in planets)
            {
                if (planetView.planetModel.rotatesAround)
                {
                    planetView.rigidbodyOfPlane.AddTorque(new Vector3(0, planetView.planetModel.rotationSpeed, 0), ForceMode.Force);
                }
                if (!planetView.tag.Equals("sun"))
                {
                    float initialVelocity = Mathf.Sqrt(sun.rigidbodyOfPlane.mass / planetView.transform.position.magnitude);
                    planetView.rigidbodyOfPlane.velocity = new Vector3(0, initialVelocity, 0);
                }
            }
        }
    }

    /// <summary>
    /// Adds x planets
    /// </summary>
    /// <param name="howMuch"></param>
    private void AddPlanets(int howMuch)
    {
        if (planetViewPrefab != null)
        {
            planets = new List<PlanetView>();
            planets.Add(sun);
            for (int i = 0; i < howMuch; i++)
            {
                PlanetView newPlanet = Instantiate(planetViewPrefab);
                xOffset += UnityEngine.Random.Range(3f, 7f);
                newPlanet.planetModel = GenerateRandomPlanetModel();
                newPlanet.transform.position = new Vector3(xOffset, 0, 0);
                newPlanet.transform.localScale = new Vector3(newPlanet.planetModel.size, newPlanet.planetModel.size, newPlanet.planetModel.size);
                newPlanet.gameObject.GetComponent<MeshRenderer>().material.color = newPlanet.planetModel.color;
                planets.Add(newPlanet);
            }
        }
    }

    /// <summary>
    /// Generates random planet model
    /// </summary>
    /// <returns></returns>
    private PlanetModel GenerateRandomPlanetModel()
    {
        PlanetModel model = new PlanetModel();
        model.lifePoints = UnityEngine.Random.Range(100, 200);
        model.rotatesAround = true;
        model.rotationSpeed = UnityEngine.Random.Range(100, 200);
        model.size = UnityEngine.Random.Range(1f, 3f);
        model.color = Color.HSVToRGB(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f));
        model.planetName = "Planet";
        return model;
    }
    #endregion
}


