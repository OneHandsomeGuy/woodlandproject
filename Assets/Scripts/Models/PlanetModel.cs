﻿using UnityEngine;
[System.Serializable]
public class PlanetModel
{
    public string planetName;
    public float lifePoints;
    public float size;
    public float rotationSpeed;
    public Color color;
    public bool rotatesAround = false;
}
